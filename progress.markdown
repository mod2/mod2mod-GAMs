Texas Water Development Board's Groundwater Availability Models
===============================================================
# Successful runs for each version of MODFLOW
*   **Aquifer**              **-1996**    **-2000**    **-2005**    **-NWT**
    Dockum                                X
    Edwards - Barton                      X            X
    Edwards - Northern                    X
    Edwards-Trinity                       X
    Gulf Coast - Central     X            X            X
    Gulf Coast - Northern                 X            X
    Gulf Coast - Southern    X
    Hueco Bolsons            X
    igbl                     X            X            X
    Lipan                    X            X            X
    Mesilla Bolsons          X            X            X
    Nacatoch                              X
    Ogalalla - Northern      X            X            X
    Carrizo-Wilcox           X            X            X
    Seymour                               X            X
    West Texas Bolsons                    X            X
    Yegua-Jackson                         X            X
    Trinity - Northern                                              X
